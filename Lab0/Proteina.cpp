/* 
 * Proteina.cpp
 */
#include <list>
#include <iostream>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"

/* constructores */
Proteina::Proteina(string nombre, string id) {
	this->nombre = nombre;
	this->id = id;
}

/* métodos get and set */
string Proteina::get_nombre() {
	return this->nombre;
}

string Proteina::get_id() {
	return this->id;
}

void Proteina::set_nombre(string nombre) {
	this->nombre = nombre;
}

void Proteina::set_id(string id) {
	this->id = id;
}

void Proteina::add_cadena(Cadena cadena) {
	// lista de nombre cadenas
	this->cadenas.push_back(cadena);
	//para oredenar se puede usar
	//sort(this->cadenas);
}

list<Cadena> Proteina::get_cadenas() {
	return this->cadenas;
}
