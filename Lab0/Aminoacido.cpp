/*
 * Aminoacido.cpp
 */
#include <list>
#include <iostream>
using namespace std;
#include "Aminoacido.h"
#include "Atomo.h"

/* constructores */
Aminoacido::Aminoacido (string nombre, int numero) {
	this->nombre = nombre;
	this->numero = numero;
}

/* métodos get and set */
string Aminoacido::get_nombre() {
	return this->nombre;
}

int Aminoacido::get_numero() {
	return this->numero;
}

void Aminoacido::set_nombre(string nombre) {
	this->nombre = nombre;
}

void Aminoacido::set_numero(int numero) {
	this->numero = numero;
}

void Aminoacido::add_atomo(Atomo atomo) {
	this->atomos.push_back(atomo);
}

list<Atomo> Aminoacido::get_atomos() {
	return this->atomos; //revisar esta linea
}
