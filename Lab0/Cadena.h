/* 
 * Cadena.h
 */
#include <list>
#include <iostream>
using namespace std;
#include "Aminoacido.h"

#ifndef CADENA_H
#define CADENA_H

class Cadena {
	private:
		string letra;
		list<Aminoacido> aminoacidos;

	public:
		/* constructores */
		Cadena (string letra);
		
		/* métodos get and set */
		string get_letra();

		void set_letra(string letra);
		void add_aminoacido(Aminoacido aminoacido);
		list<Aminoacido> get_aminoacidos();
};
#endif
