/*
 * Cadena.cpp
 */
#include <list>
#include <iostream>
using namespace std;
#include "Cadena.h"

/* constructores */
Cadena::Cadena(string letra) {
	this->letra = letra;
}

/* métodos get and set */
string Cadena::get_letra() {
	return this->letra;
}

void Cadena::set_letra(string letra) {
	this->letra = letra;
}

void Cadena::add_aminoacido(Aminoacido aminoacido) {
	this->aminoacidos.push_back(aminoacido);
}

list<Aminoacido> Cadena::get_aminoacidos() {
	return this->aminoacidos;
}
