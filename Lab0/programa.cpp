/*
 * g++ programa.cpp Proteina.cpp -o programa
 */
#include <iostream>
#include <list>
#include <stdlib.h>
#include <stdio.h>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

void imprimir_datos_proteinas(list<Proteina> proteina) {
	system("clear");
	cout << "\n\t ***PROTEINAS INGRESADAS***" << endl;
	
	for (Proteina p : proteina){
		cout << "\n\t Informacion [" << p.get_nombre() << "]" << endl;
		cout << "Nombre Proteina: " << p.get_nombre() << endl;
		cout << "ID: " << p.get_id() << endl;

		cout << "\n\tCadenas" << endl;
		for(Cadena c: p.get_cadenas() ){
			cout << "\n\tCadena: " << c.get_letra() << endl;

			cout << "\n\t\tAminoácidos" << endl;
			for(Aminoacido a: c.get_aminoacidos() ){
				cout << "\n\t\tAminoacido: " << a.get_nombre() << " " << a.get_numero() << endl;

				cout << "\n\t\t\tAtomos" << endl;
				for(Atomo at: a.get_atomos() ){
					cout << "\n\t\t\tAtomo: " << at.get_nombre() << " " << at.get_numero() << endl;
					Coordenada co = at.get_coordenada();
					cout << "\n\t\t\tCoordenadas: (" << co.get_x() << " , " << co.get_y() <<  " , " << co.get_z() << ")" << endl;
				}
			}
		}
		cout << "----------------------------------------------" << endl;
	}
	
	/*
	cout << "ID: " << p.get_id() << "Nombre Proteina: " << p.get_nombre() << endl;
	/* TODO
	 * AGREGAR CICLOS FOR PARA CADENAS, etc
	
	for (Cadena c: p.get_cadenas()){
		cout << "Cadena: " << c.get_letra() <<endl;
		for (Aminoacido a: c.get_aminoacidos()){
			cout << "Aminoacido: " << a.get_nombre() << " " << a.get_numero() << endl;
		}
	}*/
}

void leer_datos_proteinas() {
	
	list<Proteina> proteina;

	cout << "Ingrese cantidad de Proteinas: ";
	int cant_proteinas;
	cin >> cant_proteinas;
	cout << "\n\t\tComplete el formulario para 1 Proteina" << endl;
	for (int i = 0; i < cant_proteinas; i++){
		//Solicitar informacion para...
		//CADA PROTEINA
		cout << "\nIngrese ID de una Proteina: ";
		string id_proteina;
		cin >> id_proteina;
		cout << "Ingrese el NOMBRE de la Proteina: "; 
		string nombre_proteina;
		cin >> nombre_proteina;
		Proteina p = Proteina(nombre_proteina, id_proteina);
		
		//CADENAS
		cout << "\n\tIngrese la cantidad de Cadenas de esta Proteina: ";
		int cant_cadenas;
		cin >> cant_cadenas;
		for(int j = 0; j < cant_cadenas; j++){
			//CADA CADENA
			cout << "Ingrese la LETRA de una cadena: ";
			string letra_cadena;
			cin >> letra_cadena;
			//instancia una cadena
			Cadena c = Cadena(letra_cadena);
			
			//AMINOACIDOS
			cout << "\n\tIngrese la cantidad de Aminoacidos de la cadena: ";
			int cant_aminoacidos;
			cin >> cant_aminoacidos;
			for(int k = 0; k < cant_aminoacidos; k++){
				//CADA AMINOACIDO
				cout << "Ingrese el NOMBRE de un aminoacido: ";
				string nombre_aminoacido;
				cin >> nombre_aminoacido;
				cout << "Ingrese el NUMERO de este aminoacido: ";
				int numero_aminoacido;
				cin >> numero_aminoacido;
				Aminoacido aminoacid = Aminoacido(nombre_aminoacido, numero_aminoacido); //REVISAR ESTA LINEA
				
				//ATOMOS
				cout << "\n\tIngrese la cantidad de Atomos: ";
				int cant_atomos;
				cin >> cant_atomos;
				for(int l = 0; l < cant_atomos; l++){
					//CADA ATOMO
					cout << "Ingrese el NOMBRE de un atomo: ";
					string nombre_atomo;
					cin >> nombre_atomo;
					cout << "Ingrese el NUMERO de este atomo: ";
					int numero_atomo;
					cin >> numero_atomo;
					Atomo atomo = Atomo(nombre_atomo, numero_atomo);
					//COORDENADAS
					cout << "Ingrese su coordenada X: ";
					float coordenada_x;
					cin >> coordenada_x;
					cout << "Ingrese su coordenada Y: ";
					float coordenada_y;
					cin >> coordenada_y;
					cout << "Ingrese su coordenada Z: ";
					float coordenada_z;
					cin >> coordenada_z;

					Coordenada coordenada = Coordenada(coordenada_x, coordenada_y, coordenada_z);
					atomo.set_coordenada(coordenada);
					aminoacid.add_atomo(atomo);
				}
				c.add_aminoacido(aminoacid);
			}
			// Agregar cadena
			p.add_cadena(c);
		}
		proteina.push_back(p);
	}
	imprimir_datos_proteinas(proteina);
}



int main(){

	int choice;
		system("clear");
		cout << "\n\t\t     MENU DEMO Version 1.0 ";
		cout << "\n\n";
		cout << "\n\t\t\t 1. Añadir Proteina ";
		cout << "\n\t\t\t 2. Ver Proteinas ";
		cout << "\n\t\t\t 3. Salir del Programa ";
		cout << "\n\n";
		cout << "\n\t\t Ingrese su Opcion :=> ";
		cin  >> choice;

		if (choice == 1) {
			cout << "\n\n Ha seleccionado Añadir";
			cout << "\n\n";
			// TODO LLAMAR METODO
			leer_datos_proteinas();
		}else if (choice == 2) {
			cout << "\n\n Ha seleccionado Ver";
			cout << "\n\n";
			//TODO 
			cout << "\n\n OPCION NO DISPONIBLE ACTUALMENTE";
			main();
		}else if (choice == 3) {
			cout << "\n\n\tGracias por usar este programa. Adios.";
			exit(0);
		}else if (choice != 3) {
		 cout << "\n\n\n \t\t\t Opcion Invalida. Vuelva a intentarlo...";
		 cout << "\n\n";
		main();
	}
	 /*

	//list<Proteina> proteina;

	// instancia una proteina (EJEMPLO)
	Proteina p1 = Proteina("AXC2", "PROTEIN-NAME");

	proteina.push_back(p1);

	// instancia una cadena
	Cadena a = Cadena("A");
	// agregar aminoacido a cadena A
	a.add_aminoacido(Aminoacido("ALA", 1));
	a.add_aminoacido(Aminoacido("HIS", 2));

	// Agregar cadena
	p1.add_cadena(a);

	Cadena b = Cadena("B");
	b.add_aminoacido(Aminoacido("CYS", 5));
	b.add_aminoacido(Aminoacido("LEU", 7));
	b.add_aminoacido(Aminoacido("ANA", 13));	
	
	// agregar otra cadena
	p1.add_cadena(b);

	// imprime datos proteina
	imprimir_datos_proteinas(p1);
*/
	return 0;
}