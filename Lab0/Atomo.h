/*
 * Atomo.h
 */
#include <iostream>
#include <list>
using namespace std;
#include "Coordenada.h"

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
	private:
		string nombre;
		int numero;
		Coordenada coordenada;

	public:
		/* constructores */
		Atomo();
		Atomo(string nombre, int numero);
		
		/* métodos get and set */
		string get_nombre();
		int get_numero();
		Coordenada get_coordenada();

		void set_nombre(string nombre);
		void set_numero(int numero);
		void set_coordenada(Coordenada coordenada);
		
};
#endif
