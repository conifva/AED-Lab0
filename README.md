# AEDLab0

# Lab0-UI

Dado el diagrama de clases adjunto (*diagrama_clases.png*), escriba un programa en C++ que  implemente dichas clases. El programa debe permitir el ingreso  de una lista de proteínas con su información asociada.
Considere además:
- por cada clase escribir su archivo .cpp y .h
- utilizar Makefile para la compilación.
- implemente un menu básico para indicar las acciones.

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta "Lab0" y ejecutar por terminal:
```
g++ programa.cpp Proteina.cpp -o programa
make
./programa
```

# Diagrama
![](diagrama_clases.png)

# Acerca de
El inicio del programa esta vinculado a un menú principal en donde se encuentra la función de ingresar datos de una o varias proteinas y mostrar estos al usuario por terminal.
Las variables ligadas a este menú no se encuentran detalladas en el diagrama de clases.

# Errores
- No estan considerados los errores por parte del usuario al momento de ingresar informacion. Por ejemplo, si se ingresan caracteres del tipo string (u otro por el estilo) cuando se solicitan de tipo numerico se producirá un error y cerrará el programa.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela